from flask import Flask, render_template, flash, request,send_file
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, DateField
from datetime import datetime
 
import csv
from flask import make_response
import requests

from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from multiprocessing import Pool

import time
from multiprocessing.dummy import Pool as ThreadPool 
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
app.config['TIMEOUT'] = 50
app.config['IP'] = 'https://atqrhman:9GHST5QOORA0A2ATDLE3J481@23.94.140.11:9346'
app.config['headers']={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'}


class ReusableForm(Form):
	name = TextField('Name:', validators=[validators.required()])
	start_date = DateField('Start Date',default=datetime.today())
	end_date = DateField('End Date',default=datetime.today())


def chunks(l, n):
	"""Yield successive n-sized chunks from l."""
	for i in xrange(0, len(l), n):
		yield l[i:i + n]

def extract_ebay(start_date, end_date , one_product, more_then):
	try:
		one_product = BeautifulSoup(one_product , 'html.parser')
		today = datetime.now()
		link_of_one_product = one_product.find('h3',attrs={'class':'lvtitle'}).find('a').get('href')
		date_of_one_product = one_product.find('span',attrs={'class':'tme'}).find('span').get_text()
		date_of_one_product = datetime.strptime(date_of_one_product , '%b-%d %H:%M')
		if date_of_one_product.month <= today.month:
			date_of_one_product = date_of_one_product.replace(year=today.year, hour=0 , minute=0)
		else:
			date_of_one_product = date_of_one_product.replace(year=today.year-1 , hour=0 , minute=0)

		date_of_one_product = date_of_one_product.date()
		print(start_date , date_of_one_product)
		if start_date <= date_of_one_product <= end_date:
			link_of_one_product = link_of_one_product + '&nordt=true&rt=nc&orig_cvip=true'
			request_of_one_product = requests.get(link_of_one_product, timeout=app.config['TIMEOUT'] ).content
			soup_of_one_product = BeautifulSoup(request_of_one_product, 'html.parser')
			try:
				sold = soup_of_one_product.find('span',attrs={"class":{'qtyTxt vi-bboxrev-dsplblk vi-qty-fixAlignment feedbackON'}})
				sold = sold.findChildren("a" , recursive=True)[0]
				
				sold = int(sold.get_text().strip()[:-5])
				if sold >= more_then:
					return (soup_of_one_product.find('h2',attrs={"itemprop":"gtin13"}).get_text())
			except Exception as e:
				print (e)
				print('logging-' , 'error finding UPC of ' + link_of_one_product)
				return None
		elif start_date > date_of_one_product:
			return False
	except Exception as e:
		print (e)
		pass

@app.route("/", methods=['GET', 'POST'])
def hello():
	form = ReusableForm(request.form)
	stores = []
	break_loop = False
	if request.method == 'POST':
		try:
			n = 50
			k = 50	
			pool = ThreadPool(n) 
			start_time = time.time()
			store_name = request.form['store']
			username=request.form['name']
			days = int(request.form['date'])
			end_date =datetime.now().date()
			start_date = datetime.now().date() - timedelta(days=days)
			highest_price = float(request.form['highest_price'])
			lowest_price = float(request.form['lowest_price'])
			more_then=int(request.form['more_then'])
			
			list_of_UPC = []
			all_href = []
			results = []

			i = 0
			url = 'https://www.ebay.com/sch/m.html?item=323633286871&LH_Complete=1&LH_Sold=1&_ssn={username}&rt=nc'.format(username=username)
			while True:
				print(i)
				i+=1
				try:
					if url == 'javascript:;':
						break
					# page = requests.get(url, timeout=app.config['TIMEOUT'])
					page = requests.get(url, timeout=app.config['TIMEOUT'], proxies={'https':app.config['IP']})
					
					soup = BeautifulSoup(page.content, 'html.parser')
					if "Access Denied" in soup.find('h1').get_text():
						flash('Error! Could not connect to Ebay Access Denied') 
						return render_template('hello.html',form=form)
				except Exception as e:
					print (e) 
					# flash('Error! Could not connect to Ebay')
					# return render_template('hello.html',form=form)


				all_links = soup.find_all('li',attrs={'class':'sresult lvresult clearfix li'})
				# all_links = [all_links[i:i + k] for i in range(0, len(all_links), k)]
				print (username)

				# for some_links in all_links:
				# 	print (len(some_links))
				params = [(start_date, end_date ,str(i), more_then) for i in all_links]
				results = pool.starmap(extract_ebay,params)
				list_of_UPC += results
				if False in results:
					list_of_UPC = [x for x in list_of_UPC if x != False]
					list_of_UPC = [x for x in list_of_UPC if x is not None ]
					list_of_UPC = [x for x in list_of_UPC if x is not 'Does not apply' ]
					break_loop = True
					break



				try:
					url = soup.find('td',attrs={ 'class':"pagn-next"}).find('a')['href']
				except Exception as e:
					print ('************',e)
					break
			
			product_link_list = []
			if store_name =='walmart':
				params = [(lowest_price, highest_price ,upc) for upc in list_of_UPC]
				product_link_list += pool.starmap(extract_walmart ,params)
				

			elif store_name == 'home_depot':
				params = [(lowest_price, highest_price ,upc) for upc in list_of_UPC]
				product_link_list += pool.starmap(extract_home_depot ,params)
			
			product_link_list = [x for x in product_link_list if  x != None ]
			product_link_list  = '\r\n'.join(product_link_list)

			output = make_response(product_link_list)
			output.headers["Content-Disposition"] = "attachment; filename={username}.txt".format(username=username + '_' + store_name)
			output.headers["Content-type"] = "text/txt"
			pool.terminate()
			pool.join()
			print("--- %s seconds for n= %s---" % ((time.time() - start_time) , n))
			return output

		except Exception as e:
			print(e)
			return render_template('hello.html',form=form)
	return render_template('hello.html',form=form)
		 




def extract_walmart(lowest_price , highest_price , upc):
	try:
		walmart_link = 'https://www.walmart.com'
		search_link = walmart_link + "/search/?query={upc}".format(upc=upc)
		# page = requests.get(search_link, timeout=app.config['TIMEOUT'],proxies={'https':app.config['IP']})
		page = requests.get(search_link, timeout=app.config['TIMEOUT'])
		soup = BeautifulSoup(page.content, 'html.parser')
		price = float(soup.find('span',attrs={'class':'price-group'}).get_text()[1:])
		print ('price',price)
		if lowest_price <= price <= highest_price:
			product_link = soup.find('a',attrs={'class':'product-title-link line-clamp line-clamp-2'}).get('href')
			product_link = walmart_link + product_link
			if soup.find('div', attrs={'class':'marketplace-sold-by'}) == None:
				return product_link
			else:
				return None
	except Exception as e:
		print('logging-' , 'error in ' + search_link)
		print(e)
		return None


def extract_home_depot(lowest_price , highest_price , upc):
	try:
		hd_link = 'https://www.homedepot.com'
		search_link = hd_link + "/s/{upc}?NCNI-5".format(upc=upc)
		print (search_link)
		page = requests.get(search_link, headers=app.config['headers'], proxies={'https':app.config['IP']} ,timeout=app.config['TIMEOUT'])
		# page = requests.get(search_link, headers=headers, timeout=app.config['TIMEOUT'])
		soup = BeautifulSoup(page.content, 'html.parser')
		if soup.find('input',attrs={'id':'ciItemPrice'}) == None:
			search_link = hd_link + soup.find('div',attrs={'class':'pod-plp__description js-podclick-analytics'}).find('a')['href']
			page = requests.get(search_link, headers=app.config['headers'], proxies={'https':app.config['IP']} ,timeout=app.config['TIMEOUT'])
			# page = requests.get(search_link, headers=headers, timeout=app.config['TIMEOUT'])
			soup = BeautifulSoup(page.content, 'html.parser')
				
		price = float(soup.find('input',attrs={'id':'ciItemPrice'})['value'])
		if lowest_price <= price <= highest_price:
			return search_link
		else:
			return None
	except Exception as e:
		print('logging-' , 'error in ' + search_link)
		print(e)
		return None


# Home Depot 


# bargainhunter108
# sippisale
# nicolasanddave-3
# productsdiscounted
# usa_home_goods
# bestvaluesellers

# Walmart username 

# bettyzbargainz
# shipfourfree
# katy_vaits
# bestofferdeals4you
# thereupsquad
# treasure23
# beksenterprises